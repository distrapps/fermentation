<?php

/**
 * Template Name: Blog
 */

get_header();

?>


<div id="wrapper" class="singlepage">

  <?php
  	$image = get_field('cover_image');
  	if( !empty($image) ): ?>

  <div class="coverbox" style="background-image:url('<?php echo $image['url']; ?>')">

  <?php endif; ?>

    <div class="outerbox">
      <div class="innerbox clearfix">
        <div class="container">
          <div class="boxtitle">
            <div class="innertitle">
              <h1>Blog</h1>
              <div class="subtext">Home | <span class="subactive"> Blog</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- end coverbox -->


  <div class="block ctngridov" data-aos="fade-up" data-aos-anchor-placement="center-bottom">
    <div class="container">
      <div class="ctngridov__outer">
        <div class="row">


          <?php

          $post_objects = get_field('content_blog');

          if( $post_objects ): ?>

            <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
              <?php setup_postdata($post); ?>
              <div class="col-md-4 col-sm-6 col-xs-12">
                <a href="<?php the_permalink(); ?>">
                  <div class="ctngridov__inner">
                    <div class="ctngridov__img">
                      <div class="ctngridov__overlay">
                        <div class="ctngridov__overlay--text">
                          <h5>Baca</h5>
                        </div>
                      </div>
                      <?php
                        if ( has_post_thumbnail() ) {
                          $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                          echo '<img src="'.$image[0].'" data-id="'.$post->ID.'">';
                        }
                      ?>
                    </div>
                    <div class="ctngridov__desc">
                      <div class="ctngridov__desctittle">
                        <h4><?php the_title(); ?></h4>
                        <p>Post by <?php the_author(); ?>, <?php the_time('F jS, Y'); ?></p>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
          <?php endif; ?>


        </div>
      </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>
