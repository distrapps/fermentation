<?php

/**
 * Template Name: About
 */

get_header();

?>


<div id="wrapper" class="singlepage">

  <?php
  	$image = get_field('cover_image');
  	if( !empty($image) ): ?>

  <div class="coverbox" style="background-image:url('<?php echo $image['url']; ?>')">

  <?php endif; ?>

    <div class="outerbox">
      <div class="innerbox clearfix">
        <div class="container">
          <div class="boxtitle">
            <div class="innertitle">
              <h1>Tentang Kami</h1>
              <div class="subtext">Home | <span class="subactive"> Tentang Kami</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- end coverbox -->


</div>


<?php get_footer(); ?>
