=== Fermentation Template ===
Contributors: Distrapps
Tags: simple, company profile
Requires at least: 4.8
Stable tag: v1.0.5
Version: 1.0.5
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Fermentation template


== Installation ==
This section describes how to install the child theme and get it working.

== Changelog ==

= 1.0.5 =
* Adjust blog pages
* Adjust main menu
* Adjust style sitewide
* Adjust product page, woocommerce settings
* Adjust footer

= 1.0.4 =
* Adjust blog detail
* Adjust style sitewide

= 1.0.3 =
* Adjust contact & blog post

= 1.0.2 =
* Add cover image for pages sitewide

= 1.0.1 =
* Add custom fields for pages sitewide

= 1.0.0 =
* initial release
