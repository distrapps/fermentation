// Mobile Menu
jQuery(function(){
  jQuery('#mobinav ul.top-nav li').each(function() {
    if(jQuery('.sub-menu',this).length) {
      jQuery('> a',this).addClass('toggle-submenu');
      jQuery('.sub-menu',this).each(function() {
        jQuery(this).addClass('level-'+jQuery(this).parents('#mobinav .sub-menu').length);
      });
    }
  });
  jQuery('#mobinav ul.monav li.has-submenu',this).append('<i class="fa fa-angle-down"></i>');

  //subnav toggle
  jQuery('ul.monav i').on('click', function(e){
    e.preventDefault();
    jQuery('#mobinav li').not(jQuery(this).parents('li')).removeClass('shrink');
    jQuery(this).closest('li').toggleClass('shrink');
  });
});
function openNav() {
  document.getElementById("mobinav").style.width = "100%";
}
function closeNav() {
  document.getElementById("mobinav").style.width = "0";
}

var app = {
  init: function() {
    app.headerScroll();
    app.sliderDeals()
    app.sliderMain();
    app.aosPlu();
  },
  headerScroll: function(){
    var header = jQuery("#section-top");
  	jQuery(window).on('load ready resize scroll',function(){
  		var scroll = jQuery(window).scrollTop();
  		if (scroll >= 140) {
  			header.removeClass('navbar-default').addClass("scrolled");
  		} else {
  			header.removeClass("scrolled").addClass('navbar-default');
  		}
  	});
  },
  sliderDeals: function(){
    jQuery('.product-slide').owlCarousel({
        loop:true,
        lazyLoad:true,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                loop: true,
            },
            480:{
                items:1,
                nav:false,
                loop: true
            },
            768:{
                items:1,
                nav:false,
                loop:true
            }
        }
    })
  },
  sliderMain: function(){
    jQuery('#slider-main').owlCarousel({
      loop:true,
      nav:true,
      autoplay:true,
      autoplayTimeout:6000,
      autoplayHoverPause:true,
      smartSpeed: 2500,
      navText : ["<i class='icon-left-open-big'></i>","<i class='icon-right-open-big'></i>"],
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:1
          }
      }
    })
  },
  aosPlu: function(){
    AOS.init({
        offset: 170,
        startEvent: 'load',
    });
  }
};
jQuery(document).ready(function($){
app.init();
})
