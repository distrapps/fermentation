<?php

/**
 * Template Name: Contact
 */

get_header();

?>


<div id="wrapper" class="singlepage">

  <?php
  	$image = get_field('cover_image');
  	if( !empty($image) ): ?>

  <div class="coverbox" style="background-image:url('<?php echo $image['url']; ?>')">

  <?php endif; ?>

    <div class="outerbox">
      <div class="innerbox clearfix">
        <div class="container">
          <div class="boxtitle">
            <div class="innertitle">
              <h1>Kontak</h1>
              <div class="subtext">Home | <span class="subactive"> Kontak</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- end coverbox -->

  <div class="block form-contact" data-aos="zoom-in">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <?php if(get_field('contact_info')): ?>
          <div class="contact-desc">
            <div class="contact-item">
              <span><i class="demo-icon icon-location"></i></span>
              <div class="box-tittle">
                <h5>Alamat</h5>
              </div>
              <div class="box-text">
                <p><?php the_field('contact_info'); ?></p>
              </div>
            </div>
          </div>
          <?php endif; ?>

          <div class="contact-desc">
          <div class="contact-item">
            <span><i class="demo-icon icon-phone"></i></span>
            <div class="box-tittle">
              <h5>No Telephone</h5>
            </div>
            <div class="box-text">

              <?php

              	// check if the repeater field has rows of data
              	if( have_rows('contact_phone') ):

              		// loop through the rows of data
              		while ( have_rows('contact_phone') ) : the_row(); ?>

                  <p><?php the_sub_field('phone'); ?></p>

                <?php
              	  endwhile;
              		else :
              			// no rows found
              		endif;
              ?>

            </div>
          </div>
        </div>

          <div class="contact-desc">
          <div class="contact-item">
            <span><i class="demo-icon icon-mail"></i></span>
            <div class="box-tittle">
              <h5>Email</h5>
            </div>
            <div class="box-text">

              <?php

              	// check if the repeater field has rows of data
              	if( have_rows('contact_email') ):

              		// loop through the rows of data
              		while ( have_rows('contact_email') ) : the_row(); ?>

                  <p><?php the_sub_field('email'); ?></p>

              <?php
              	endwhile;
              		else :
              			// no rows found
              		endif;
              ?>
            </div>
          </div>
        </div>

        </div>

        <div class="col-md-6 col-sm-6">
          <div class="contact-form">
            <?php echo do_shortcode( '[contact-form-7 id="140" title="Contact form 1"]' ); ?>
          </div>
        </div>
      </div>
    </div>
  </div>



</div>


<?php get_footer(); ?>
