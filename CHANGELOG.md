## 06 September 2018 - v1.0.5 ##
* Adjust blog pages
* Adjust main menu
* Adjust style sitewide
* Adjust product page, woocommerce settings
* Adjust footer

## 04 September 2018 - v1.0.4 ##
* Adjust blog detail
* Adjust style sitewide

## 30 August 2018 - v1.0.3 ##
* Adjust contact & blog post

## 26 August 2018 - v1.0.2 ##
* Add cover image for pages sitewide

## 26 August 2018 - v1.0.1 ##
* Add custom fields for pages sitewide

## 18 August 2018 - v1.0.0 ##
* Initial Release
