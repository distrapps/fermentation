<?php

/*
 * Template Name: Featured Article
 * Template Post Type: post, page, product
 */

get_header();

?>


<div id="wrapper" class="singlepage">

  <?php
  	$image = get_field('page_cover', 'options');
  	if( !empty($image) ): ?>

  <div class="coverbox" style="background-image:url('<?php echo $image['url']; ?>')">

  <?php endif; ?>

    <div class="outerbox">
      <div class="innerbox clearfix">
        <div class="container">
          <div class="boxtitle">
            <div class="innertitle">
              <h1>Blog</h1>
              <div class="subtext">Home | <span class="subactive"> Blog</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- end coverbox -->

  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <?php
            if (have_posts()): while (have_posts()) : the_post(); ?>

            <div class="imgblog">
                <?php the_post_thumbnail( 'gallery-slide-main' );?>
            </div>

            <div class="headingblog">
                <h1><?php the_title();?></h1>
                <span>Post by <?php the_author(); ?>, <?php the_time('F jS, Y'); ?></span>
            </div>

            <div class="contentblog">
                <?php the_content(); ?>
            </div>


            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
      </div>

      <div class="blogrelated">
          <h4>Other Blog</h4>
          <?php example_cats_related_post() ?>
      </div>


    </div>
  </div>





</div>


<?php get_footer(); ?>
