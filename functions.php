<?php

require_once get_template_directory() . '/wp-bootstrap-navwalker.php';
register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'header-menu' ),
) );

function mytheme_add_woocommerce_support(){
    add_theme_support('woocommerce', array(
        'thumbnail_image_width' => 150,
        'single_image_width' => 300,
        'product_grid' => array(
            'default_rows' => 3,
            'min_rows' => 2,
            'max_rows' => 8,
            'default_columns' => 4,
            'min_columns' => 2,
            'max_columns' => 5,
            ),
        ));
}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');


function mytheme_add_woocommerce_zooming(){
    add_theme_support('wc-product-gallery-zoom');
    //add_theme_support('wc-product-gallery-lightbox');
    //add_theme_support('wc-product-gallery-slider');
}
add_action('wp', 'mytheme_add_woocommerce_zooming', 99);

function cl (){
    the_excerpt();
}
add_action('woocommerce_after_shop_lopp_item_title', 'cl', 40);

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
}

function example_cats_related_post() {

    $post_id = get_the_ID();
    $cat_ids = array();
    $categories = get_the_category( $post_id );

    if(!empty($categories) && is_wp_error($categories)):
        foreach ($categories as $category):
            array_push($cat_ids, $category->term_id);
        endforeach;
    endif;

    $current_post_type = get_post_type($post_id);
    $query_args = array(

        'category__in'   => $cat_ids,
        'post_type'      => $current_post_type,
        'post_not_in'    => array($post_id),
        'posts_per_page'  => '3'


     );

    $related_cats_post = new WP_Query( $query_args );

    if($related_cats_post->have_posts()):
         while($related_cats_post->have_posts()): $related_cats_post->the_post(); ?>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
                <?php the_content(); ?>
            </div>
        <?php endwhile;

        // Restore original Post Data
        wp_reset_postdata();
     endif;

}


if (function_exists('add_theme_support'))
    {

        // Add Thumbnail Theme Support
        add_theme_support('post-thumbnails');
        add_image_size('large', 700, '', true);
        add_image_size('medium', 320, 200, true);
        add_image_size('small', 120, '', true);
        add_image_size('full');
        add_image_size( 'admin-list-thumb', 80, 80, true);
        add_image_size( 'album-grid', 450, 450, true );
        add_image_size('gallery-slide', 900, 500, true);
        add_image_size('custom-size', 900, 300, true);
        add_image_size('gallery-slide-main', 1920, 1080, true);

    }

function my_theme_enqueue_styles() {

    $parent_style = 'fermentation-style';
    wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js', array(), false, true );
    wp_enqueue_script('jsflexslider', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider.js', array(), false, true );
    wp_enqueue_script('jsaos', 'https://unpkg.com/aos@2.3.1/dist/aos.js', array(), false, true );
    wp_enqueue_script('jsvendor', get_stylesheet_directory_uri() .  '/asset/js/vendor/vendor.min.js', array(), false, true );
    wp_enqueue_script('jsglobal', get_stylesheet_directory_uri() .  '/asset/js/global.js', array(), false, true );

    wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), '3.3.7','all' );
    wp_enqueue_style('aos', 'https://unpkg.com/aos@2.3.1/dist/aos.css', array(), '2.3.1','all' );
    wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.0.4/css/all.css', array(), '5.0.4','all' );
    wp_enqueue_style('flexslider', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css', array(), '2.7.1','all' );
    wp_enqueue_style('fontello', get_stylesheet_directory_uri() . '/asset/fontello/css/marker.css', array(), '2.7.1','all' );
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles', 60 );



    require_once ( get_stylesheet_directory() . '/inc/plugin-update-checker/plugin-update-checker.php' );
        $updateChecker = Puc_v4_Factory::buildUpdateChecker(
            'https://bitbucket.org/distrapps/fermentation',
            __FILE__,
            'fermentation'
        );

        $updateChecker->setAuthentication( array(
            'consumer_key' => 'fgRqxkNVeWkCxpumeT',
            'consumer_secret' => 'eJJTb6YYSGjVKZ6LszVgrGPejR79BKH8',
        ) );

        $updateChecker->setBranch( 'master' );

    ?>
