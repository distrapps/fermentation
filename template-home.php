<?php

/**
 * Template Name: Homepage
 */

get_header();

?>

<style media="all">
  .flexi .slides > li {
    padding-bottom: 60%!important;
  }
</style>

<div id="wrapper" class="homepage">
  <div class="section main-slider">
    <?php

    $images = get_field('home_slider');

    if( $images ): ?>
      <div id="slider" class="flexslider">
        <ul class="slides">
          <?php foreach( $images as $image ): ?>
            <li style="background-image: url('<?php echo $image['url']; ?>')"></li>
          <?php endforeach; ?>
        </ul>
      </div>
    <?php endif; ?>
  </div><!-- end .main-slider -->

  <div class="block block__intro" data-aos="fade-up" data-aos-duration="500">
    <?php while ( have_posts() ) : the_post(); ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 custom--col">
          <div class="intro--img r">
            <?php if( get_field('home_intro_image') ): ?>
            	<img src="<?php the_field('home_intro_image'); ?>" />
            <?php endif; ?>
          </div>
        </div>
        <div class="col-md-6 custom--col">
          <div class="intro--desc">
            <h2><?php the_field('home_intro_title'); ?></h2>
            <p><?php the_field('home_intro'); ?></p>
            <button type="submit" class="btn-basic ">See Map</button>
          </div>
        </div>
      </div>
    </div>
    <?php endwhile; // end of the loop. ?>
  </div><!-- end .block__intro -->

  <?php
  	$image = get_field('home_product_background');
  	if( !empty($image) ): ?>

    <div class="block block__product" style="background-image: url('<?php echo $image['url']; ?>')">
      <?php endif; ?>
      <div class="container">
        <div class="outer-content" data-aos="fade-up" data-aos-duration="500">
          <div class="shead">
            <h2>Produk Terpopuler</h2>
          </div>

          <div class="row">
            <?php

            	// check if the repeater field has rows of data
            	if( have_rows('home_product_popular') ):

            		// loop through the rows of data
            		while ( have_rows('home_product_popular') ) : the_row(); ?>

                <div class="col-md-4 col-sm-4 col-xs-12 ">
                  <div class="inner-content">
                    <a href="#">
                      <div class="content-img">
                        <div class="overlay">
                        </div>
                        <img src="<?php the_sub_field('home_product_popular_image'); ?>" alt="">
                      </div>
                      <div class="content-desc">
                        <div class="content-desctittle">
                          <h4><?php the_sub_field('home_product_popular_title'); ?></h4>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>

            		<?php
            		endwhile;
            		else :
            			// no rows found
            	endif;
            ?>

          </div><!-- end .row -->

        </div><!-- end .outer-content -->
      </div><!-- end .container -->
    </div><!-- end .block-product -->

  <div class="block ctegoriesld" data-aos="zoom-in">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="ctnsld-desc">
            <h2><?php the_field('home_category_title'); ?></h2>
            <p><?php the_field('home_category_intro'); ?></p>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="block main-slider">
            <?php

            $images = get_field('home_category_slider');

            if( $images ): ?>
                <div id="slider" class="flexslider flexi">
                    <ul class="slides">
                        <?php foreach( $images as $image ): ?>
                            <li style="background-image: url('<?php echo $image['url']; ?>')">
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
          </div>
          <!-- end .main-slider -->
        </div>

      </div>
    </div>
  </div>
</div>

  <script type="text/javascript" charset="utf-8">
    jQuery(window).load(function() {
      jQuery('.flexslider').flexslider({
        controlNav: false
      });
    });
  </script>

<?php get_footer(); ?>
